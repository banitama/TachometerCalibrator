#include <FreqMeasure.h>
//#include <analogComp.h>
#include <FrequencyTimer2.h>
#include <AccelStepper.h>
//#include <eRCaGuy_Timer2_Counter.h>

//Tachometer Calibrator
//I Putu Banitama Supartha (2015)
//GNU GPL v3 License
//Specification:
//maximum RPM simulated = 99999

#include <LiquidCrystal.h>
#include <EEPROM.h>
#define SERIAL_DBG 1

//navigational button, Nokia style
const int btn_mode_pin=2;
const int btn_adjust_pin=3;
//button debouncing variables
//all units in milliseconds 
unsigned long last_debounce_time = 0;
unsigned long debounce_delay = 40;

//LCD pins
const int lcd_rs_pin=A0;
const int lcd_en_pin=A1;
const int lcd_d4_pin=A2;
const int lcd_d5_pin=A3;
const int lcd_d6_pin=A4;
const int lcd_d7_pin=A5;
LiquidCrystal lcd(lcd_rs_pin, lcd_en_pin, lcd_d4_pin, lcd_d5_pin, lcd_d6_pin, lcd_d7_pin);

//LCD custom logo
byte arrow_up[8] = {
  0b00000,
  0b00100,
  0b01110,
  0b10101,
  0b00100,
  0b00100,
  0b00100,
  0b00000
};

byte arrow_cycle[8] = {
  0b00010,
  0b11001,
  0b00010,
  0b00000,
  0b00000,
  0b01000,
  0b10011,
  0b01000
};

byte check_icon [8] = {
  0b00000,
  0b00000,
  0b00000,
  0b00001,
  0b00010,
  0b10100,
  0b01000,
  0b00000
};

byte arrow_up_idx = 0;
byte arrow_cycle_idx = 1;
byte check_icon_idx = 2;

//Tachometer LED pins
const int tach_led_pin = FREQUENCYTIMER2_PIN;
#define tach_led_port PORTB
int tach_led_state = LOW;
//Overhead in branching
const unsigned long overhead_micros = 14;

//Strobe tachometer photodiode pin
const int tach_photo_pin=8;

//LED RPM initial setting
const int max_simulated_rpm = 99999;
float simulated_rpm = 1200;
float measured_rpm = 0;
unsigned long period_millis, period_micros;
const long max_precise_period_us = 32768;
unsigned long led_on_time, led_off_time;
//This flag will change to false if change is made
//to simulated RPM and the led_on_time and
//led_off_time is not updated.
boolean tach_change_applied = true;

//timing variables
unsigned long prevMillis,prevMicros;

//Display main screen onto LCD
void print_main_screen (float rpm) 
{
  String str1=F("   ");
  String str3=F(" rpm");
  String str2 = String (rpm,2);
  String content = str1 + str2 + str3;
  lcd.clear();
  lcd.print(content.c_str());
  lcd.setCursor(0,1);
  lcd.print(F("ADJUST      MODE"));
}

//Convert RPM setting into rotational period in ms
unsigned long RPMtoPeriod_ms (unsigned long rpm)
{
  if (rpm==0) return 50;
  else return (60000/rpm);
}

//Convert RPM setting into rotational period in us
unsigned long RPMtoPeriod_us (unsigned long rpm)
{
  if (rpm==0) return 50000;
  else return (60000000/rpm);
}

int tach_led_get (void)
{
  return tach_led_state;
}

void tach_led_set(void)
{
  
}

unsigned long powint (unsigned long base, unsigned long y)
{
  unsigned long val=base;
 for(unsigned long z=0;z<=y;z++)
 {
   if(z==0)
     val=1;
   else
     val=val*base;
 }
 return val;
}

//Set LED on-time and off_time 
//Units set in ms if RPM<30000, otherwise set in us 
void LED_setup(void)
{
  unsigned long maximum_on_time_us = 100000;
  unsigned long maximum_half_on_time_us = 500000;
  int maximum_duty_cycle = 50;
  int target_duty_cycle = 20;
  unsigned long minimum_on_time_us = 60000000/max_simulated_rpm;
  unsigned long minimum_half_on_time_us = 30000000/max_simulated_rpm;

  period_micros = RPMtoPeriod_us (simulated_rpm);
  Serial.print(F("Period:"));
  Serial.print(period_micros);
  Serial.print(F(" us\n"));
  if (period_micros < max_precise_period_us){
    FrequencyTimer2::enable();
    FrequencyTimer2::setPeriod (period_micros);
    //the frequency resolution is limited,
    //so let's get real freq here
    period_micros = FrequencyTimer2::getPeriod();
    Serial.print(F("Real Period:"));
    Serial.print(period_micros);
    Serial.print(F(" us\n"));
    //don't forget to set the real rpm here
    simulated_rpm = 60000000.0/period_micros;
  }
  else {
    FrequencyTimer2::disable();
    pinMode (tach_led_pin, OUTPUT);
    led_on_time = target_duty_cycle * period_micros / 100;
    led_off_time = period_micros - led_on_time;
    Serial.print(F("On time:"));
    Serial.print(led_on_time);
    Serial.print(F(" us, off time:"));
    Serial.print(led_off_time);
    Serial.print(F("us\n"));
  }
}

//Load RPM value from EEPROM.
void load_eeprom_rpm(void)
{
  unsigned long eeprom_rpm;
  EEPROM.get(0,eeprom_rpm);
  Serial.print(F("Get RPM from EEPROM: "));
  Serial.print(eeprom_rpm);
  Serial.print(F("\n"));
  simulated_rpm=eeprom_rpm;
}

void store_eeprom_rpm (void)
{
  EEPROM.put(0,(unsigned long)simulated_rpm);
  Serial.print (F("Stored RPM to EEPROM\n"));
}

//operation mode definition
byte current_opmode;

#define PHOTOTACHOMETER 0
#define STROBOSCOPE 1
#define CONTACTTACHOMETER 2
//how many operating mode this device give?
#define OPMODE_COUNT 3
//where the opmode is stored on the 
#define OPMODE_ADDRESS 8

void store_eeprom_opmode (byte opmode)
{
  EEPROM.put(OPMODE_ADDRESS,opmode);
  Serial.print (F("Stored opmode to EEPROM\n"));
}

byte load_eeprom_opmode (void)
{
  byte eeprom_opmode;
  EEPROM.get(OPMODE_ADDRESS,eeprom_opmode);
  Serial.print(F("Get mode from EEPROM: "));
  Serial.print(eeprom_opmode);
  Serial.print(F("\n"));
  return eeprom_opmode;
}

//take RPM value from button reading
//the method of setting is similar to the old digital 
//watch:set from the largest number to the smallest number

void RPM_setup (void)
{
  int cursor_position=4;
  String rpm_string =String ((unsigned long)simulated_rpm);
  bool all_set = false;

  //create upper-row text
  lcd.setCursor (0,0);
  lcd.print(F("Speed:       rpm"));
  
  //get all decimal places
  byte simulated_rpm_array[5];
  unsigned long simulated_rpm_remainder=simulated_rpm;
  Serial.print (F("RPM setting: "));
  for (int i=4;i>=0;i--)
  {
    simulated_rpm_array[i] = simulated_rpm_remainder/powint(10,i);
    simulated_rpm_remainder -= powint(10,i) * simulated_rpm_array[i];
    Serial.print(simulated_rpm_array[i]);
  }
  
  Serial.print (F("\n"));
  //do not exit until all setting is stored
  int dec_pos_value;
  String lcd_rpm_val;
  bool need_lcd_val_update = true;
  while (!all_set)
  { 
    if(need_lcd_val_update)
    {
      //print all digits
      lcd_rpm_val=String("");
      for (int i=4;i>=0;i--)
      {
        lcd_rpm_val = lcd_rpm_val + simulated_rpm_array[i];
      }
      lcd.setCursor(6,0);
      lcd.print(lcd_rpm_val.c_str());
      
      //get decimal digit
      dec_pos_value = simulated_rpm_array[cursor_position];
      
      //display lower row
      lcd.setCursor (0,1);
      lcd.print ("+             >>");
      //RPM cursor edit position
      lcd.setCursor(10-cursor_position,1);
      lcd.write(arrow_up_idx);
    }

    if (digitalRead(btn_mode_pin) == LOW) 
    {//mode button pressed
      //debounce
      delay(debounce_delay);
      if (digitalRead(btn_mode_pin) == LOW) 
      {
        while (digitalRead(btn_mode_pin) == LOW);
        if (cursor_position == 0){
          //Convert decimal array into decimal number
          simulated_rpm=0;
          for (int i=0; i<5;i++)
          {
            simulated_rpm+=powint(10,i)*simulated_rpm_array[i];
          }
          if (simulated_rpm == 0)
          {
            //display error message
            //set to default RPM
            simulated_rpm = 1200;
          }
          all_set = true;
          store_eeprom_rpm();
        }
        else 
        {
          cursor_position--;
          need_lcd_val_update = true;
        }
      }
    }
    else if (digitalRead(btn_adjust_pin) == LOW) 
    {//adjust button pressed
      //debounce
      delay(debounce_delay);
      if (digitalRead(btn_adjust_pin) == LOW) 
      {
        //wait until button is released
        while (digitalRead(btn_adjust_pin) == LOW);
        simulated_rpm_array[cursor_position]++;
        simulated_rpm_array[cursor_position]=simulated_rpm_array[cursor_position]%10;
        need_lcd_val_update = true;
      }
    }
    else
    {
      need_lcd_val_update = false;
    }
  }
}

void display_current_mode (void)
{
  if (current_opmode == PHOTOTACHOMETER)
        {
          lcd.setCursor (0,0);
          lcd.print (F("                "));
          lcd.setCursor (0,0);
          lcd.print (F("Phototachometer"));
        }
        else if (current_opmode == STROBOSCOPE)
        {
          lcd.setCursor (0,0);
          lcd.print (F("                "));
          lcd.setCursor (0,0);
          lcd.print (F("  Stroboscope"));
        }
        else if (current_opmode == CONTACTTACHOMETER)
        {
          lcd.setCursor (0,0);
          lcd.print (F("                "));
          lcd.setCursor (0,0);
          lcd.print (F("Mech. Tachometer"));
        }
}

void mode_setup(void)
{
  bool all_set = false;
  lcd.clear();
  lcd.setCursor(15,1);
  lcd.write(arrow_cycle_idx);
  lcd.setCursor (0,1);
  lcd.write (check_icon_idx);
  display_current_mode();
  while (!all_set)
  {
    if (digitalRead(btn_mode_pin) == LOW) 
    {//mode button pressed
      //debounce
      delay(debounce_delay);
      if (digitalRead(btn_mode_pin) == LOW) 
      {
        while (digitalRead(btn_mode_pin) == LOW);
        //cycle mode
        current_opmode++;
        current_opmode = current_opmode % OPMODE_COUNT;
        //display cycled mode
        display_current_mode();
      }
    }
    if (digitalRead(btn_adjust_pin) == LOW) 
    {//mode button pressed
      //debounce
      delay(debounce_delay);
      if (digitalRead(btn_adjust_pin) == LOW) 
      {
        while (digitalRead(btn_adjust_pin) == LOW);
        //exit
        all_set = true;
      }
    }
  }
}

//Interrupt handler for taking care of 
//button press during main loop
volatile boolean btn_adj_flag;
volatile boolean btn_mode_flag;
void adjust_int_handler (void)
{
  btn_adj_flag = true;
}

void mode_int_handler (void)
{
  btn_mode_flag = true;
}

void set_btn_interrupt(void)
{
  attachInterrupt(digitalPinToInterrupt(btn_adjust_pin),
                  adjust_int_handler,
                  FALLING);
  attachInterrupt(digitalPinToInterrupt(btn_mode_pin),
                  mode_int_handler,
                  FALLING);
}

void reset_btn_interrupt(void)
{
  detachInterrupt(digitalPinToInterrupt(btn_adjust_pin));
  detachInterrupt(digitalPinToInterrupt(btn_mode_pin));
}

void soft_reset(void)
{
  asm volatile ("  jmp 0");
}

void setup() {
  // put your setup code here, to run once:

  //LCD initialization
  lcd.begin(16,2);
  lcd.createChar(arrow_up_idx, arrow_up);
  lcd.createChar (arrow_cycle_idx, arrow_cycle);
  lcd.createChar (check_icon_idx, check_icon);

  //I/O setup
  pinMode (btn_mode_pin, INPUT_PULLUP);
  pinMode (btn_adjust_pin, INPUT_PULLUP);
  prevMicros=micros();
  
  //debugging purpose
  Serial.begin(115200);
  //store_eeprom_rpm ((unsigned long) 1200);
  //store_eeprom_opmode ((byte) STROBOSCOPE);

  //load configuration from eeprom
  current_opmode = load_eeprom_opmode();
  
  if (current_opmode == PHOTOTACHOMETER)
  {
    //I/O setup
    pinMode (tach_led_pin, OUTPUT);
    //pinMode (tach_photo_pin, INPUT);
  
    lcd.setCursor(0,0);
    lcd.print(F("Phototachometer"));
    lcd.setCursor (2,1);
    lcd.print(F("Calibrator"));  
    delay(1000);
    load_eeprom_rpm();
    LED_setup();
    print_main_screen(simulated_rpm);
  }
  else if (current_opmode == STROBOSCOPE)
  {
    //I/O setup
    pinMode (tach_led_pin, INPUT);
    FreqMeasure.begin();
    
    lcd.setCursor(2,0);
    lcd.print(F("Stroboscope"));
    lcd.setCursor (2,1);
    lcd.print(F("Calibrator"));  
    delay(1000);
    print_main_screen (0);
  }
  //default case for unitialized device
  else {
    store_eeprom_opmode(PHOTOTACHOMETER);
    delay(1000);
    soft_reset();
  }

  set_btn_interrupt();
  btn_adj_flag = false;
  btn_mode_flag = false;
}

double sum=0;
int count=0;

void loop() {
  // put your main code here, to run repeatedly:
  //run the blinking portion if frequency is low enough
  if (current_opmode == PHOTOTACHOMETER && period_micros >= max_precise_period_us &&
      !(btn_adj_flag || btn_mode_flag))
  {
    if ((tach_led_get()==HIGH)&&
      ((micros()-prevMicros)>=led_on_time)){
      tach_led_port &= ~(1 << 3);
      prevMicros=micros();
      tach_led_state = LOW;
    }
    else if ((tach_led_get()==LOW)&&
      ((micros()-prevMicros)>=led_off_time)){
      tach_led_port |= 1 << 3;
      prevMicros=micros();
      tach_led_state = HIGH;
    }
  }
  else if (current_opmode == STROBOSCOPE)
  {
    if (FreqMeasure.available()) {
      //Serial.print (F("Measurement results are available.\n"));
      // average several reading together
      sum = sum + FreqMeasure.read();
      count = count + 1;
      //average the calculation, or display the instantaneous result 
      //update calculation every 1 second or more
      if ((count > 5) && (micros() - prevMicros > 1000000)) {
        float frequency = FreqMeasure.countToFrequency(sum / count);
        measured_rpm = frequency * 60;
        Serial.print (F("Measured RPM: "));
        Serial.print (measured_rpm);
        Serial.print (F("\n"));
        //update display every 1 sec.
        if (micros() - prevMicros > 1000000)
        {
          print_main_screen (measured_rpm);
        }
        sum = 0;
        count = 0;
        prevMicros = micros();
      }
    }
    //timeout after 5 seconds
  }
  //go to setting mode if a certain button is pressed
  if (current_opmode == PHOTOTACHOMETER && btn_adj_flag)
  {
    reset_btn_interrupt();
    Serial.print (F("Entering RPM change menu\n"));
    //debounce
    delay(debounce_delay);
    if (digitalRead(btn_adjust_pin) == LOW) 
    {
      //wait until button is released
      while (digitalRead(btn_adjust_pin) == LOW);
    }
    //turn off LED for safety
    tach_led_port &= ~(1 << 3);
    tach_led_state = LOW;
    RPM_setup();
    //tach_change_applied = true;
    LED_setup();
    lcd.clear();
    lcd.print(F(" Please wait... "));
    delay(1000);
    print_main_screen(simulated_rpm); 
    set_btn_interrupt();
    btn_adj_flag = false;
    btn_mode_flag = false;
  }
  else if (btn_mode_flag)
  {
    reset_btn_interrupt();
    Serial.print (F("Entering mode change menu\n"));
    //debounce
    delay(debounce_delay);
    if (digitalRead(btn_adjust_pin) == LOW) 
    {
      //wait until button is released
      while (digitalRead(btn_adjust_pin) == LOW);
    }
    tach_led_port &= ~(1 << tach_led_pin);
    tach_led_state = LOW;
    mode_setup();
    lcd.clear();
    lcd.print(F(" Please wait... "));
    store_eeprom_opmode(current_opmode);
    delay(1000);
    soft_reset();
  }
}
